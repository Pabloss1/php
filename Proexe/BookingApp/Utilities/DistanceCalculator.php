<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


use Proexe\BookingApp\Offices\Models\OfficeModel;

class DistanceCalculator {

	/**
	 * @param array  $from
	 * @param array  $to
	 * @param string $unit - m, km
	 *
	 * @return mixed
	 */
	public function calculate( $from, $to, $unit = 'm' ) {
		return \sqrt(\pow($to[1]-$from[1], 2) + \pow($to[0]- $from[0], 2)) * 111 * ($unit === 'm' ? 1000: 1);
	}

    /**
     * @param array $from
     *
     * @return string
     */
	public function findClosestOffice( $from ) {
        return OfficeModel::query()
            ->select('id', 'name')
            ->orderByRaw('SQRT(POWER(lat - ?, 2) + POWER(lng - ?, 2)) asc', $from)
            ->limit(1)
            ->get();
	}

	// RAW SQL:
    // SELECT o.id, o.name  FROM offices o
    // ORDER BY SQRT(POWER(o.lat - 14.12232322, 2) + POWER(o.lng - 8.12232322, 2))
    // LIMIT 1
}
