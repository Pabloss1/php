<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 *
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


use Carbon\Carbon;
use Carbon\CarbonInterval;
use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface
{
    public function calculate($bookingDateTime, $responseDateTime, $officeHours)
    {
        return $bookingDateTime->diffFiltered(CarbonInterval::minute(), function (Carbon $date) use ($officeHours) {
            $dayData = $officeHours[$date->dayOfWeek];
            $timeToCalculate = Carbon::createFromTimestamp($date->timestamp);
            return !$dayData['isClosed'] &&
                $timeToCalculate->between(
                    Carbon::createFromFormat(
                        'Y-m-d H:i',
                        $timeToCalculate->format('Y-m-d') . ' ' . $dayData['from']
                    ),
                    Carbon::createFromFormat(
                        'Y-m-d H:i',
                        $timeToCalculate->format('Y-m-d') . ' ' . $dayData['to']
                    )
                );
        }, $responseDateTime);
    }
}
