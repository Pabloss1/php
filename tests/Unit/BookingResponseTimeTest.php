<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Proexe\BookingApp\Utilities\ResponseTimeCalculator;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookingResponseTimeTest extends TestCase
{
    /**
     * @test
     * @dataProvider dataHoursProvider
     *
     * @param $expected
     * @param $from
     * @param $to
     * @param $workingHours
     */
    public function testTask4($expected, $from, $to, $workingHours)
    {
        $this->assertEquals($expected, (new ResponseTimeCalculator())->calculate($from, $to, $workingHours));
    }

    public function dataHoursProvider()
    {
        return [
            [
                'expected' => 0,
                'from' => Carbon::create(),
                'to' => Carbon::create(),
                [
                    'isClosed' => false,
                    'workingHourFrom' => '8:00',
                    'workingHourTo' => '16:00',
                ]
            ],
        ];
    }
}
